import React, { Component } from "react";
import { Menu, Avatar, Drawer } from 'antd';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { UserOutlined } from '@ant-design/icons';
import Pokeball from "../../assets/img/pokeball.png";
import "./menu.scss";
import { Select } from 'antd';
import axios from "axios";

const { Option } = Select;

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      pokemonsArray: [],
      avatarSelected: ""
    }
  }


  componentDidMount() {
    axios({
      method: "GET",
      url: "http://127.0.0.1:8000/pokemons"
    }).then((res) => {
      this.setState({ pokemonsArray: res.data })
    })
      .catch((err) => {
        console.error(err);
      });
  }

  state = {
    current: 'mail',
  };
  onClose = () => {
    this.setState({ visible: false });
  };

  handleClick = e => {
    console.log('click ', e);
    this.setState({ current: e.key });
  };

  avatarSelect = () => {
    console.log("clicked");
    this.setState({ visible: true })
  }
  handleChange = (e) => {
    console.log(e);
    this.setState({ avatarSelected: e })

    // AXIOS POST USER
  }
  render() {

    const avatars = this.state.pokemonsArray.filter(pokemons => pokemons.id_pok === 25 ? pokemons : pokemons.id_pok === 1 ? pokemons : pokemons.id_pok === 4 ? pokemons : pokemons.id_pok === 7 ? pokemons : pokemons.id_pok === 39 ? pokemons : "");
    console.log(avatars);
    const { current } = this.state;
    return (
      <div id="menu">
        <img src={Pokeball} width="64" height="64" />
        <Menu onClick={this.handleClick} selectedKeys={[current]} mode="horizontal">
          <Menu.Item key="pokedex">
            <Link to="/pokedex">Pokedex</Link>
          </Menu.Item>
          <Menu.Item key="users">
            <Link to="/dresseurs">Dresseurs</Link>
          </Menu.Item>
          <Menu.Item key="auth">
            <Link to="/">Authenticate</Link>
          </Menu.Item>
        </Menu>
        <Avatar size={64} icon={<img src={this.state.avatarSelected} />} id="avatar" onClick={this.avatarSelect} />
        <Drawer title="Profile avatar" placement="right" onClose={this.onClose} visible={this.state.visible}>
          <Avatar size={64} icon={<img src={this.state.avatarSelected} />} id="sidebar-avatar" />
          <Select defaultValue={<img src={Pokeball} width={"40"} height={"40"} />} style={{ width: 120 }} onChange={(e) => this.handleChange(e)}>
            {
              avatars.length > 0 ? avatars.map((avatar, i) => {
                console.log(i);
                return (
                  <Option key={i} value={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${avatar.id_pok}.png`}><img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${avatar.id_pok}.png`} /></Option>
                )
              })
                : <div>No data for avatars</div>
            }
          </Select>
        </Drawer>
      </div>
    );
  }

}

export default Navbar;