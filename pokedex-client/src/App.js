import React, { Component } from "react";
import logo from './logo.svg';
import './App.css';
import 'react-table-6/react-table.css'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Auth from './View/auth';
import 'antd/dist/antd.css';
import Dashboard from "./View/dashboard/dashboard";
import Pokedex from "./View/pokedex/pokedex"
import Dresseurs from "./View/dresseurs/dresseurs";
class App extends Component {
  componentDidMount() {
  }
  render() {

    return (
      <Router>
        <Dashboard/>
          <Switch>
            <Route path="/pokedex">
              <Pokedex/>
            </Route>
            <Route path="/dresseurs">
              <Dresseurs />
            </Route>
            <Route path="/">
              <Auth />
            </Route>
          </Switch>
      </Router >
    );
  }
}

export default (App);
