import React, { Component } from "react";
import axios from "axios";
import "./pokedex.scss";
import { Drawer, Button } from 'antd';
import ReactTable from 'react-table-6'


class Pokedex extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pokedexArray: [],
            pokedexTypes: [],
            visible: true,
            pokemonData: "",
            pokedexStats: [],
            pokedexEvolve: [],
            pokedexWeak: []
        }
    }

    componentDidMount() {
        axios({
            method: 'GET',
            url: "http://127.0.0.1:8000/pokemons"
        }).then((res) => {
            this.setState({ pokedexArray: res.data })
        })
            .catch(function (error) {
                console.log(error);
            })
        axios({
            method: 'GET',
            url: "http://127.0.0.1:8000/types"
        }).then((res) => {
            this.setState({ pokedexTypes: res.data })
        })
            .catch(function (error) {
                console.log(error);
            })
        axios({
            method: 'GET',
            url: "http://127.0.0.1:8000/stats"
        }).then((res) => {
            this.setState({ pokedexStats: res.data })
        })
            .catch(function (error) {
                console.log(error);
            })
        axios({
            method: 'GET',
            url: "http://127.0.0.1:8000/evolutions"
        }).then((res) => {
            this.setState({ pokedexEvolve: res.data })
        })
            .catch(function (error) {
                console.log(error);
            })
        axios({
            method: 'GET',
            url: "http://127.0.0.1:8000/weaknesses"
        }).then((res) => {
            this.setState({ pokedexWeak: res.data })
        })
            .catch(function (error) {
                console.log(error);
            })
    }

    onClose = () => {
        this.setState({ visible: false });
    };

    pokemonSelected = id => {
        this.setState({ visible: true })
        axios({
            method: "GET",
            url: `http://127.0.0.1:8000/pokemon/${id}`
        }).then((res) => {
            this.setState({ pokemonData: res.data[0] });
        }).catch((err) => {
            console.error(err);
        })
    }
    rowClick = (rowInfo) => {
        return {
            onClick: e => {
                // console.log(rowInfo?.original?);
            }
        }

    }

    render() {
        const columns = [{
            Header: 'Sprites',
            accessor: 'id_pok',
            Cell: props =>
                <img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${props.value}.png`} />

        },
        {
            Header: "Pokemon",
            accessor: "nom_pok",
            filterable: true
        },
        {
            id: "type1",
            Header: "Types",
            accessor: d => d.types,
            Cell: props => {
                return <p>{props.value.type1} {props.value.type2 !== "" ? " // " + props.value.type2 : ""}</p>
            }
        },
        {
            id: "Stats",
            Header: "Statistiques",
            accessor: d => d.stats,
            Cell: props => {
                return <p>HP: {props.value?.hp} // Attack: {props.value?.attack} // Defense: {props.value?.defense} // Special Attack: {props.value?.sp_attack} //  Special Defense: {props.value?.sp_defense} // Speed: {props.value?.speed} // </p>
            }
        },
        {
            id: "Evolve",
            Header: "Evolutions",
            accessor: d => d.evolve,
            Cell: props => {
                console.log(props.value);
                return <p>Evolue au niveau : {props.value?.lvl_evol_pok}  <img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${props.value?.id_pok_evol}.png`} /></p>
            }
        }
        ];
        const test = this.state.pokedexArray?.map(
            (poke, i) => {
                poke.types = { type1: this.state.pokedexTypes[i]?.type1, type2: this.state.pokedexTypes[i]?.type2 };
                poke.stats = this.state.pokedexStats[i]
                poke.evolve = this.state.pokedexEvolve[i]
                poke.weak = this.state.pokedexWeak[i]
                return poke
            }
        )
        return (
            <div id="pokedex">
                <h2>Pokedex</h2>
                <ReactTable
                    data={this.state.pokedexArray}
                    columns={columns}
                    getTrProps={this.rowClick}
                />
                <Drawer title={this.state.pokemonData.nom_pok} placement="right" onClose={this.onClose} visible={this.state.visible}>
                    <div className="pokemonSidebar" >
                        <img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${this.state.pokemonData.id_pok}.png`} />
                        {
                            <p>
                                ID #{this.state.pokemonData.id_pok}
                                {this.state.pokemonData.nom_pok}
                            </p>
                        }
                    </div>
                </Drawer>
            </div >
        );
    }

}

export default Pokedex;