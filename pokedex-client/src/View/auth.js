import React, { Component } from 'react';
import { Form, Input, Button, Checkbox } from 'antd';
import "./auth.scss";
import axios from "axios";

class Auth extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            outputLogin: "",
            outputRegister: ""
        }
    }
    onFinishLogin = (values) => {
        console.log('Success:', values);
        // axios({
        //     method: "GET",
        //     url: `http://127.0.0.1:8000/login`,
        //     data: {
        //         email: values.email,
        //         password: values.password
        //     }
        // })
        axios.get(`http://127.0.0.1:8000/login`, {
            email: values.email,
            password: values.password
        })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            })
    };

    onFinishFailedLogin = (errorInfo) => {
        this.setState({ outputLogin: errorInfo.errorFields[0].errors })
        var output = document.getElementById("output-error-login");
        output.style.opacity = 1;
        setTimeout(function () {
            output.style.opacity = 0;
        }, 3000);
    };

    onFinishRegister = (values) => {
        console.log('Success:', values);
        axios.post("http://127.0.0.1:8000/register",
            {
                username: values.username,
                email: values.email,
                password: values.password,
                confirm: values.confirm,
                _token: values._token
            },
            {
                headers: {
                    WithCredentials: true
                }
            }
        )
        axios({
            method: "POST",
            url: `http://127.0.0.1:8000/register`,
            data: {
                username: values.username,
                email: values.email,
                password: values.password,
                confirm: values.confirm,
                _token: values._token
            }
        });
    };

    onFinishFailedRegister = (errorInfo) => {
        this.setState({ outputRegister: errorInfo.errorFields[0].errors })
        var output = document.getElementById("output-error-register");
        output.style.opacity = 1;
        setTimeout(function () {
            output.style.opacity = 0;
        }, 3000);
    };

    render() {
        console.log(this.state.outputLogin);
        return (
            <div id="auth">
                <div id="register">
                    <h2>Register</h2>
                    <Form
                        name="basic"
                        labelCol={{ span: 10 }}
                        wrapperCol={{ span: 6 }}
                        initialValues={{ remember: true }}
                        onFinish={this.onFinishRegister}
                        onFinishFailed={this.onFinishFailedRegister}
                        autoComplete="off"
                    >
                        <Form.Item
                            label="Username"
                            name="username"
                            rules={[{ required: true, message: 'Please input your username!' }]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="Email"
                            name="email"
                            name={['user', 'email']} label="Email" rules={[{ required: true, type: 'email' }]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Password"
                            name="password"
                            rules={[{ min: 8, message: "Password must be 8 caracters at minimum" }, { required: true, message: 'Please input your password!' }]}
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item
                            name="confirm"
                            label="Confirm Password"
                            dependencies={['password']}
                            hasFeedback
                            rules={[
                                { min: 8, message: "Password must be 8 caracters at minimum" },
                                {
                                    required: true,
                                    message: 'Please confirm your password!',
                                },
                                ({ getFieldValue }) => ({
                                    validator(_, value) {
                                        if (!value || getFieldValue('password') === value) {
                                            return Promise.resolve();
                                        }
                                        return Promise.reject(new Error('The two passwords that you entered do not match!'));
                                    },
                                }),
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>
                        <Form.Item wrapperCol={{ offset: 8, span: 16 }}
                            type="hidden" htmlType="submit" name="_token" value={'{{csrf_token()}}'}
                        >
                            <Input >
                            </Input>
                        </Form.Item>
                        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                    <div id="output-error-register">{this.state.outputRegister}</div>
                </div>
                <div id="login">
                    <h2>Login</h2>
                    <Form
                        name="basic"
                        labelCol={{ span: 9 }}
                        wrapperCol={{ span: 6 }}
                        initialValues={{ remember: true }}
                        onFinish={this.onFinishLogin}
                        onFinishFailed={this.onFinishFailedLogin}
                        autoComplete="off"
                    >
                        <Form.Item
                            label="Email"
                            name="email"
                            rules={[{ required: true, type: 'email' }]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Password"
                            name="password"
                            rules={[{ min: 8, message: "Password must be 8 caracters at minimum" }, { required: true, message: 'Please input your password!' }]}
                        >
                            <Input.Password />
                        </Form.Item>
                        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                    <div id="output-error-login">{this.state.outputLogin}</div>
                </div>
            </div>
        )
    }
}

export default Auth;