<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PokedexController;
use App\Http\Controllers\TypesController;
use App\Http\Controllers\EvolutionsController;
use App\Http\Controllers\WeaknessesController;
use App\Http\Controllers\StatsController;
use App\Http\Controllers\UsersController;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/users', [UsersController::class, "users"]);

Route::get("/pokemons", [PokedexController::class, "pokemons"]);
Route::get("/pokemon/{id}", [PokedexController::class, "pokemonSelected"]);


Route::get("/types", [TypesController::class, "types"]);
Route::get("/evolutions", [EvolutionsController::class, "evolutions"]);
Route::get("/stats", [StatsController::class, "stats"]);
Route::get("/weaknesses", [WeaknessesController::class, "weak"]);

// Route::post("/register", function (Request $request) {
//     $token = $request->user()->createToken($request->token_name);

//     return ['token' => $token->plainTextToken];
// });
// Route::update("/avatar",[UsersController::class, "avatar"]);
Route::get("/login", [UsersController::class, "login"]);
Route::post("/register", [UsersController::class, "register"]);