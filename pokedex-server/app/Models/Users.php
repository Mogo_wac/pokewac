<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// use Laravel\Sanctum\HasApiTokens;

class Users extends Authenticatable
{
    use HasFactory;
    protected $table = "users";
    // use HasApiTokens, HasFactory, Notifiable;
}
