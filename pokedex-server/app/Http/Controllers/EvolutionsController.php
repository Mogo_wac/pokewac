<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Evolutions;


class EvolutionsController extends Controller
{
    public function evolutions(){
        return Evolutions::all();
    }
}
