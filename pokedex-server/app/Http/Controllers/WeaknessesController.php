<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Weaknesses;

class WeaknessesController extends Controller
{
    public function weak(){
        return Weaknesses::all();
    }
}
