<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pokedex;
use App\Http\Controllers\Controller;

class PokedexController extends Controller
{
    public function pokemons(){
        return Pokedex::all();
    }

    public function pokemonSelected($id){
        $tostr = strval($id);
        return Pokedex::where("id_pok", $id)->get();
    }
}
