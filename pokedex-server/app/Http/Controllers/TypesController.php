<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Types;

class TypesController extends Controller
{
    //
    public function types(){
        return Types::all();
    }
}
