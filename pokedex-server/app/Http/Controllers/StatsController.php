<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Stats;

class StatsController extends Controller
{
    public function stats(){
        return Stats::all();
    }
}
